# ruby streamer.rb out.json

require "yajl"
require "eventmachine"
require "eventmachine-tail"

class Streamer < EventMachine::FileTail
  def initialize(path, startpos=-1)
    super(path, startpos)
    @parser = Yajl::Parser.new(:symbolize_keys => true)
    @parser.on_parse_complete = method(:object_parsed)
  end

  def object_parsed(obj)
    puts "data: #{obj} \n\n"
    File.open('/stream/result.log', 'a') do |file|
      file.write obj.inspect
    end
  end

  def receive_data(data)
    @parser << data
  end
end

def main(args)
  if args.length == 0
    puts "Usage: #{$0} <path> [path2] [...]"
    return 1
  end

  $topic = $0 || "default"

  EventMachine.run do
    args.each do |path|
      EventMachine::file_tail(path, Streamer)
    end
  end
end

exit(main(ARGV))
