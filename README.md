Have you ever tried to send your database activity logs to an event system? Get the latest changes so you could manage your cache better or even trigger other events based on data changes.
In PostgreSQL with the Logical Decoding feature, you could easily get the latest changes every time something changed in your database. Here we are going to set up the Logical Decoding first and then create a script to push these changes to Kafka, Redis, or any stream-processing platforms for further usage.


[Read more about it here](https://azolf.medium.com/process-postgresql-changes-with-logical-replication-and-ruby-a4454cac529a)

